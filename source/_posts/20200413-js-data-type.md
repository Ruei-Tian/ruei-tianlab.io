---
title: What is Closure?
date: 2020-04-12 14:18:26
tags: 
- JS
- Basic Knowledge
---

# 什麼是閉包 (Closure)
[MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures) 定義
a closure gives you access to an outer function’s scope from an inner function. In JavaScript, closures are created every time a function is created, at function creation time.

<!-- more -->

# closure 的形成和 Scope & Scope Chain 概念有關


## Scope

閉包的形成牽涉到變數宣告位置和 Scope(作用域)的概念。

![](https://res.cloudinary.com/css-tricks/image/upload/c_scale,w_1000,f_auto,q_auto/v1502482479/one-way-glass_l5dg2g.png)

#### Scope 就像單面鏡，裡面看得到外面、外面看不到裡面
→可以由內向外找尋變數，由外向內則會出現Error: is not defined


#### Global Scope
* 變數宣告位置: function 之外，程式碼內任一位置。
* 變數使用範圍: 全程式碼內、包含 function 內。

#### Function Scope
* 變數宣告位置: Function 內
* 變數使用範圍: 侷限在 {} 中

```
function sayHello () {
  const hello = 'Hello CSS-Tricks Reader!'
  console.log(hello)
}

sayHello() // 'Hello CSS-Tricks Reader!'
console.log(hello) // Error, hello is not defined

```

```
function first () {
  const firstFunctionVariable = `I'm part of first`
}

function second () {
  first()
  console.log(firstFunctionVariable) // Error, firstFunctionVariable is not defined
}
```

![](https://res.cloudinary.com/css-tricks/image/upload/c_scale,w_936,f_auto,q_auto/v1502482500/glass-layers_iohiy2.png)

## Closure

雖然 MDN 定義每個 function 都是閉包，但在目前的使用上比較能感覺到閉包的存在會是在當 function 內再包了另一個 function 的時候。

* 舉例

![](https://i.imgur.com/iimTtpF.png)

> 雖然在宣告變數 tianCost 時，已經將 buyItems 執行過並將其內的 function 傳遞給 tianCost，此時預期 buyItems 這個 function 已經執行過並會將相關資訊釋放掉。
> 但是透過傳遞 100 參數給予 tianCost 時，發現這個原本包在內層的 function 已經將 myMoney 變數關在自己的 scope ({}) 裡面了!
> 
# closure 的特點在於翻轉了 garbage collection 機制
>myMoney 之所以能夠被存起來的原因在於，buyItems執行時將 myMoney 傳入 return 出的 function 中，使變數 myMoney 被程式認為持續被使用中，因此不會被 garbage collection 回收。 

#### 這樣有什麼好處 ?

* 舉例 : 自己的錢自己花

![](https://i.imgur.com/L82E0bz.png)

### 用一段話來說明閉包

閉包是一種現象，他讓 JS 也有了 Private variable。

 
