---
title: CSS Flex Box
date: 2020-04-12 14:18:26
tags: 
- CSS
- Basic Knowledge
---

# 1.Display and Flex

* 每個 HTML 元素都有預設的 display 值，通常是 inline 或 block。

<!-- more -->
## inline
<div style="background-color: #ebe6e6">I am father
    <a href="#" 
    style="height: 100px;
    font-size: 50px;
    background-color: pink">a inline</a>
    <span 
    style="height:100px;
    font-size: 20px;
    background-color: pink">span inline</span>
</div>

* 高寬無法自訂，標籤內文字或圖片的高寬就是行內元素的高寬。

## block
<div style="background-color: #ebe6e6">I am father
    <h3 
    style="height: 
    100px;font-size: 20px;
    background-color: pink">h3 block</h3>
    <p 
    style="height: 100px;
    font-size: 50px;
    background-color: pink">p block</p>
</div>

* 加入後會從新的一行開始，特性是會自動撐滿容器的高度和寬度。

## inline block
<div style="background-color: #ebe6e6">I am father
    <a href="#" 
    style="display: inline-block;
    height:100px;
    font-size: 20px;
    background-color: pink">a inline-block</a>
</div>

<br/>
<div style="background-color: #ebe6e6">I am father
    <p 
    style="display: inline-block;
    height: 100px;
    font-size: 50px;
    background-color: pink">p inline-block</p>
</div>

* 綜合上面的特性，加入後不會斷行、但可以自訂高寬。

## Flexbox
* 重點 : 父元素 (外容器) 與子元素 (內元件) 的屬性要區分清楚。

### 屬於父元素的屬性


| display: flex | justify-content | align-items | flex-direction | flex-wrap |
| ------------- | --------------- | ----------- | -------------- | --------- |


### display: flex

<div style="
display: flex;
background-color: #9dab86;
width:350px;
height:350px;
border: 5px solid #c9753d">
    
<div style="
    background-color: #e6a157;
    width:100px;
    height:100px;
    border: 5px solid #9dab86"> 1</div>
<div style="
    background-color: #eb8242;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">2</div>
<div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">3</div>

</div>

<br/><br/>


#### 基本模型概念
* main axis (主軸) : 預設由左至右。
* cross axis (交錯軸) : 預設由上至下。
![](https://miro.medium.com/max/1037/1*yyoInkpBgFK_o6kx1BqjBQ.jpeg)

<br/><br/>

###  justify-content
#### 元素在 main axis (主軸)上的對齊方式。

![https://wcc723.github.io/css/2017/07/21/css-flex/](https://miro.medium.com/max/900/1*9wyOI_OdpLNjxPmUPMbthg.png)

###  align-items
#### 元素在 cross axis (交錯軸)上的對齊方式。

![https://wcc723.github.io/css/2017/07/21/css-flex/](https://miro.medium.com/max/900/1*nLPQOiuKDUc3FmnkpQtnZg.png)

![image alt](https://www.oxxostudio.tw/img/articles/201501/20150130_1_06.jpg)




### flex-direction - 設定 main axis 方向
`flex-direction: row` 預設值，由左到右，從上到下。
<div style="
display: flex;
flex-direction: row;
background-color: #9dab86;
width:350px;
height:350px;
border: 5px solid #c9753d">
    
<div style="
    background-color: #e6a157;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">1</div>
<div style="
    background-color: #eb8242;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">2</div>
<div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">3</div>

</div>
<br/><br/>

`flex-direction: row-reverse` 由右到左 ( 與 row 相反 )，從上到下。

<div style="
display: flex;
flex-direction: row-reverse;
background-color: #9dab86;
width:350px;
height:350px;
border: 5px solid #c9753d">
    
<div style="
    background-color: #e6a157;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">1</div>
<div style="
    background-color: #eb8242;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">2</div>
<div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">3</div>

</div>

<br/><br/>

`flex-direction: column` 從上到下，再由左到右。

<div style="
display: flex;
flex-direction: column;
justify-content: space-between;
background-color: #9dab86;
width:350px;
height:350px;
border: 5px solid #c9753d">
    
<div style="
    background-color: #e6a157;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">1</div>
<div style="
    background-color: #eb8242;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">2</div>
<div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">3</div>

</div>

<br/><br/>

`flex-direction: column-reverse` 從下到上 ( 與 column 相反)，再由左到右。

<div style="
display: flex;
flex-direction: column-reverse;
background-color: #9dab86;
width:350px;
height:350px;
border: 5px solid #c9753d">
    
<div style="
    background-color: #e6a157;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">1</div>
<div style="
    background-color: #eb8242;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">2</div>
<div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">3</div>

</div>

### flex-wrap
<br/><br/>

`flex-wrap: nowrap` 預設值，單行、可能擠壓子元素寬度。

<div style="
display: flex;
flex-wrap: nowrap;
background-color: #9dab86;
width:350px;
height:350px;
border: 5px solid #c9753d">
    
<div style="
    background-color: #e6a157;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">1</div>
<div style="
    background-color: #eb8242;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">2</div>
<div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">3</div>
    <div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">4</div>
    <div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">5</div>
    <div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">6</div>
</div>

<br/><br/>

`flex-wrap: wrap` 在子元素總寬度大於父元素時，換行。
<div style="
display: flex;
flex-wrap: wrap;
background-color: #9dab86;
width:350px;
height:350px;
border: 5px solid #c9753d">
    
<div style="
    background-color: #e6a157;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">1</div>
<div style="
    background-color: #eb8242;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">2</div>
<div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">3</div>
    <div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">4</div>
    <div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">5</div>
    <div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">6</div>
</div>

<br/><br/>

`flex-wrap: wrap-reverse` 在子元素總寬度大於父元素時，換行，且子元素內容反轉。
<div style="
display: flex;
flex-wrap: wrap-reverse;
background-color: #9dab86;
width:350px;
height:350px;
border: 5px solid #c9753d">
    
<div style="
    background-color: #e6a157;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">1</div>
<div style="
    background-color: #eb8242;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">2</div>
<div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">3</div>
    <div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">4</div>
    <div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">5</div>
    <div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">6</div>
</div>



<br/><br/><br/>

### 屬於子元素的屬性

| flex-basis | flex-grow | order | align-self | 
| ---------- | --------- | ----- | ---------- | 

#### flex-basis
默認值為 `auto`，其值及單位設定方式同 width，且優先於 width 值採用。此時瀏覽器會端看此子元素是否有設定的 width ，若有則採用其 width ，若沒有設定則會依照子元素內容在父元素內分配大小。

<div style="
display: flex;
flex-direction: row;
background-color: #9dab86;
width:350px;
height:350px;
border: 5px solid #c9753d">
    
<div style="
    flex-basis: 20%;
    background-color: #e6a157;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">20%</div>
<div style="
    flex-basis: 30%;
    background-color: #eb8242;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">30%</div>
<div style="
    flex-basis: 50%;
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">50%</div>

</div>

<br/><br/><br/>
#### flex-grow
默認值為 `0`，元素不放大，與元素之間的空白空間分配有關，無單位。如 

1. `flex: 1;` 時，子元素們將會沿 main axis 方向、平均分配空白的空間並放大。
<br/>
<div style="
display: flex;
flex-direction: row;
background-color: #9dab86;
width:350px;
height:350px;
border: 5px solid #c9753d">
    
<div style="
    flex-grow: 1;
    background-color: #e6a157;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">2</div>
<div style="
    flex-grow: 1;
    background-color: #eb8242;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">1</div>
<div style="
    flex-grow: 1;
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">1</div>

</div>


3. 也可以使子元素依照比例分配空間，如父元素內有三個子元素，第一個子元素設定 `flex-grow:2;`，其餘兩個設定 `flex-grow: 1;`，則第一個子元素分配到空白空間的 2/4，其餘各分配到 1/4 的空白空間。

<div style="
display: flex;
flex-direction: row;
background-color: #9dab86;
width:350px;
height:350px;
border: 5px solid #c9753d">
    
<div style="
    flex-grow: 2;
    background-color: #e6a157;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">2</div>
<div style="
    flex-grow: 1;
    background-color: #eb8242;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">1</div>
<div style="
    flex-grow: 1;
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">1</div>

</div>

* 備註 : 如果只填了一個無單位的數值如 `flex: 1;` ，預設以 `flex-grow` 處理。


#### order

預設為 `0`，彈性調整子元素順序，數字小的排前面、大的排後面，可為負值。

<div style="
display: flex;
flex-wrap: nowrap;
background-color: #9dab86;
width:350px;
height:350px;
border: 5px solid #c9753d">
    
<div style="
    order:2;
    background-color: #e6a157;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">1 order2</div>
<div style="
    background-color: #eb8242;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">2 order0</div>
<div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">3 order0</div>
    <div style="
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">4 order0</div>
    <div style="
    order: -1;
    background-color: #f0cf85;
    width:100px;
    height:100px;
    border: 5px solid #9dab86">5 order-1</div>
</div>

#### align-self
個別調整子元素在 cross axis (交錯軸) 的對齊方式。

![](https://miro.medium.com/max/900/1*q9igBbRr8He89pVUPWqE2w.png)


<!--  [補充](https://zh-tw.learnlayout.com/flexbox.html) -->